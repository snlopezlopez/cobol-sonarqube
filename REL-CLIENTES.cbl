      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    RelClientes.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Programa relatop de clientes
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

       copy .\copys\SEL-ARQ-CLIENTES.cpy.

       select arq-csv-clientes assign to wid-arq-csv-clientes
           organization    is line sequential
           access mode     is sequential
           file status     is ws-resultado-acesso.

      *>===================================================================================
       data division.

       copy .\copys\FD-ARQ-CLIENTES.cpy.

       fd arq-csv-clientes.
       01 registro-csv.
           02 linha-csv                            pic x(200).
      *>===================================================================================
       working-storage section.
       78 versao                       value "2.2".
       78 delimitador-csv              value ";".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

       01 ws-campos-de-trabalho.
           02 ws-situacao-campos                   pic x(01).
               88 ws-campos-ok                     value "s" "S".
           02 ws-campo-com-erro                    pic x(50).
           02 ws-relatorio.
               03 ws-cod-inicial                   pic 9(07).
           02 ws-valida-nome                       pic x(01).
               88 ws-nome-ok                       value "s" "S".
           02 ws-separa-nomes.
               03 ws-nome-01                       pic x(40).
               03 ws-nome-02                       pic x(40).
               03 ws-nome-03                       pic x(40).
           02 ws-separa-nomes-tela.
               03 ws-nome-01-t                     pic x(40).
               03 ws-nome-02-t                     pic x(40).
               03 ws-nome-03-t                     pic x(40).
           02 ws-qtd-campos-esperados              pic 9(01).
           02 ws-ind                               pic 9(05) value zeros.
           02 ws-valida-vendedor                   pic x(01).
               88 ws-vendedor-ok                   value "s" "S".

      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division using cgi-input lnk-rotinas.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao.
            perform 2000-processamento.
            perform 3000-finalizacao.
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                             to cgi-input
            initialize                              cgi-input
            accept                                  cgi-input

            perform 1010-assign-arquivos
            perform 1011-cricao-arquivos
            perform 1012-abertuta-arquivos
            .
       1000-exit.
            exit.
      *>===================================================================================
       1010-assign-arquivos section.
       1010.

            string
               lnk-path-dat    delimited by " ",
               "ARQ-CLIENTES"  delimited by " "
            into wid-arq-clientes
            end-string

            string
               lnk-path-tmp    delimited by " ",
               "rel-clientes-" delimited by " ",
               lnk-data-hora-maquina,
               ".csv"
            into wid-arq-csv-clientes
            end-string
            .
       1010-exit.
            exit.
      *>===================================================================================
       1011-cricao-arquivos section.
       1011.

            open i-o arq-clientes
            close arq-clientes

            .
       1010-exit.
            exit.
      *>===================================================================================
       1012-abertuta-arquivos section.
       1012.

            open i-o arq-clientes
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string

                   perform 8000-mensagem
                   perform 0000-saida
            end-if
            close arq-clientes
            .
       1012-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

            evaluate f-opcao-rel-cliente
            when 0
               perform 2010-monta-tela-inicial
            when 1
               perform 2020-gerar
            when other
               move "opcao inv�lida" to ws-mensagem
               perform 8000-mensagem
            end-evaluate

            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-monta-tela-inicial section.
       2010.

       exec html
           <html><title>Relat�rio de Clientes</title>
       end-exec

            perform 9000-css-pagina
            perform 2012-monta-tabela
            perform 2013-scripts-tela

       exec html
           </html>
       end-exec
            .
       2010-exit.
            exit.
      *>===================================================================================
       2012-monta-tabela section.
       2012.

       exec html

           <input type=button name=botVoltar tabindex=999 value="<- voltar" onclick="Voltar()">

           <table :html-table>
           <tr>
               <td><h1>RELAT�RIO DE CLIENTES</h1></td>
           </tr>

       <!-- =============================================================================== -->
           <tr>
           <td>
               <fieldset><legend>Op��es</legend>
               <table>
                   <tr>
                   <td class="labelCadastro">Ordena��o:</td>
                       <td>
                       <select name=opOrdenacao tabindex=1>
                       <option value="0">-- Selecionar --  </option>
                       <option value="1">Ascendente        </option>
                       <option value="2">Decrescente       </option>
                       </select>
                       </td>
                   </tr>

                   <tr>
                   <td class="labelCadastro">Tipo de classifica��o:</td>
                       <td>
                       <select name=opTipoClassificacao onchange="ValidaClassificacao()" tabindex=2>
                       <option value="0">-- Selecionar --  </option>
                       <option value="1">Cod. Cliente      </option>
                       <option value="2">Raz�o Social      </option>
                       </select>
                       </td>
                   </tr>

               </table
               </fieldset>
           </td>
           </tr>
       <!-- =============================================================================== -->
           <tr>
           <td>
               <fieldset><legend>Sele��es</legend>
               <table>

                   <tr>
                   <td></td>
                   <td class="labelRelatorio">Inicial</td>
                   <td class="labelRelatorio">Final</td>
                   </tr>

                   <tr>
                   <td class="labelCadastro">C�digo cliente:</td>
                   <td ><input name=cdInicial type=text tabindex=11 disabled value=0></td>
                   <td ><input name=cdFinal   type=text tabindex=12 disabled value=9999999 ></td>
                   </tr>

                   <tr>
                   <td class="labelCadastro">Raz�o Social:</td>
                   <td><input name=txtRelNome type=text tabindex=13 disabled></td>
                   </tr>

                   <tr>
                   <td class="labelCadastro">C�digo de Vendedor:</td>
                   <td><input name=cdVendedor type=text tabindex=20 value=0></td>
                   </tr>

               </table>
               </fieldset>
           </td>
           </tr>
       <!-- =============================================================================== -->
           <tr>
           <td>
               <input name=botGerar    type=button tabindex=30 value="Gerar"    onclick="Gerar()">
               <input name=botCancelar type=button tabindex=31 value="Cancelar" onclick="Cancelar()">
           </td>
           </tr>

           </table>

       end-exec
            .
       2012-exit.
            exit.
      *>===================================================================================
       2013-scripts-tela section.
       2013.

       exec html
       <script>

           function Cancelar() {
               DisabledBotoes()
               document.formMenus.opcaoRelCliente.value = 0;
               document.formMenus.submit();
           }

           function Gerar() {
               DisabledBotoes()
               document.formMenus.opcaoRelCliente.value = 1;
               document.formMenus.submit();
           }

           function DisabledBotoes(){
               document.all.botGerar.disabled = true;
               document.all.botCancelar.disabled = true;

           }

           function ValidaClassificacao() {
               var indOp = document.formMenus.opTipoClassificacao.selectedIndex;
               if (indOp == 1) {
                   document.formMenus.cdInicial.disabled = false;
                   document.formMenus.cdFinal.disabled = false;
                   document.formMenus.txtRelNome.disabled = true;
               } else if (indOp == 2) {
                   document.formMenus.cdInicial.disabled = true;
                   document.formMenus.cdFinal.disabled = true;
                   document.formMenus.txtRelNome.disabled = false;
               }
           }

           function Voltar(){
               document.formMenus.opcaoMenus.value = 0;
               document.formMenus.submit();
           }

       </script>
       end-exec

            .
       2013-exit.
            exit.
      *>===================================================================================
       2020-gerar section.
       2020.
            move "n"                               to ws-situacao-campos
            perform 2021-valida-campos-tela
            if not ws-campos-ok
                   string "Filtros inv�lidos, erro: ", ws-campo-com-erro
                       into ws-mensagem
                   end-string
                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            initialize                             ws-relatorio
            if   f-opOrdenacao = 1     *> Ascendente
                 perform 2030-csv-ascendente
            end-if

            if   f-opOrdenacao = 2       *> Descendente
                 perform 2040-csv-descendente
            end-if

            string "Arquivo csv gerado com sucesso, pasta tmp"
                into ws-mensagem
            end-string
            perform 8000-mensagem
            perform 2010-monta-tela-inicial
            .
       2020-exit.
            exit.
      *>===================================================================================
       2021-valida-campos-tela section.
       2021.
            if f-opOrdenacao = 0
               move "Ordena��o n�o foi selecionada" to ws-campo-com-erro
               exit section
            end-if

            if f-opTipoClassificacao = 0
               move "Tipo de classifica��o n�o foi selecionada" to ws-campo-com-erro
               exit section
            end-if

            if f-cdInicial > f-cdFinal
               move "C�digo inicial n�o pode ser maior que final" to ws-campo-com-erro
               exit section
            end-if

            move "s"                               to ws-situacao-campos
            .
       2021-exit.
            exit.
      *>===================================================================================
       2030-csv-ascendente section.
       2030.
            open input arq-clientes
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string

                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            move 0                                 to ws-cod-inicial
            move ws-cod-inicial                    to cod-cliente
            perform 2032-loop-grava-csv-asc until ws-eof-arquivo
            close arq-clientes
            .
       2030-exit.
            exit.
      *>===================================================================================
       2031-grava-csv section.
       2031.
            initialize                             registro-csv
            open extend arq-csv-clientes
            if ws-resultado-acesso = 05 *> cabe�alho
               string
                   "C�digo"        delimited by " ", delimitador-csv,
                   "cnpj"          delimited by " ", delimitador-csv,
                   "razao-social"  delimited by " ", delimitador-csv,
                   "latitude"      delimited by " ", delimitador-csv,
                   "longitude"     delimited by " ", delimitador-csv,
                   "cod.vendedor"  delimited by " ", delimitador-csv
               into linha-csv
               end-string
               write                               registro-csv
            end-if

            string
               cod-cliente     , delimitador-csv,
               cnpj            , delimitador-csv,
               razao-social    , delimitador-csv,
               latitude        , delimitador-csv,
               longitude       , delimitador-csv,
               FK-cod-vendedor , delimitador-csv
            into linha-csv
            end-string

            if   (f-opTipoClassificacao = 1) *>cod cliente
            and  (cod-cliente >= f-cdInicial)
            and  (cod-cliente <= f-cdFinal)
                  perform 2051-validar-vendedor
                  if ws-vendedor-ok
                     write                         registro-csv
                  end-if
            end-if

            if   (f-opTipoClassificacao = 2) *>por nome

                  perform 2050-validar-nome
                  if ws-nome-ok

                       perform 2051-validar-vendedor
                       if ws-vendedor-ok
                           write                         registro-csv
                       end-if
                  end-if
            end-if

            close arq-csv-clientes
            .
       2031-exit.
            exit.
      *>===================================================================================
       2032-loop-grava-csv-asc section.
       2032.

            read arq-clientes next

            if not ws-eof-arquivo
                   perform 2031-grava-csv
            end-if
            .
       2032-exit.
            exit.
      *>===================================================================================
       2040-csv-descendente section.
       2040.
            open input arq-clientes
            if not ws-operacao-ok
                   string
                       "Erro abertura do arquivo arq-clientes, status: ", ws-resultado-acesso
                   into ws-mensagem
                   end-string

                   perform 8000-mensagem
                   perform 2010-monta-tela-inicial
                   exit section
            end-if

            move 0                                 to ws-cod-inicial
            move ws-cod-inicial                    to cod-cliente
            perform 2041-ultimo-registro until ws-eof-arquivo

            move ws-cod-inicial                    to cod-cliente
            read arq-clientes
            perform 2031-grava-csv
            perform 2042-loop-grava-csv-desc until ws-eof-arquivo
            close arq-clientes
            .
       2030-exit.
            exit.
      *>===================================================================================
       2041-ultimo-registro section.
       2041.
            read arq-clientes next

            if not ws-eof-arquivo
                   move cod-cliente             to ws-cod-inicial
            end-if
            .
       2041-exit.
            exit.
      *>===================================================================================
       2042-loop-grava-csv-desc section.
       2042.

            read arq-clientes previous

            if not ws-eof-arquivo
                   perform 2031-grava-csv
            end-if
            .
       2042-exit.
            exit.
      *>===================================================================================
       2050-validar-nome section.
       2050.
            move "n"                               to ws-valida-nome
            initialize                             ws-separa-nomes
            initialize                             ws-separa-nomes-tela

            unstring razao-social delimited by " " into ws-nome-01
                                                        ws-nome-02
                                                        ws-nome-03
            end-unstring

            unstring f-txtRelNome delimited by " " into     ws-nome-01-t
                                                            ws-nome-02-t
                                                            ws-nome-03-t
            end-unstring

            if ws-nome-01-t equal spaces
               move zeros                          to ws-nome-01-t
            end-if
            if ws-nome-02-t equal spaces
               move zeros                          to ws-nome-02-t
            end-if
            if ws-nome-03-t equal spaces
               move zeros                          to ws-nome-03-t
            end-if


            if   (ws-nome-01 = ws-nome-01-t)
            or   (ws-nome-01 = ws-nome-02-t)
            or   (ws-nome-01 = ws-nome-03-t)
                  move "s"                         to ws-valida-nome
                  exit section
            end-if

            if   (ws-nome-02 = ws-nome-01-t)
            or   (ws-nome-02 = ws-nome-02-t)
            or   (ws-nome-02 = ws-nome-03-t)
                  move "s"                         to ws-valida-nome
                  exit section
            end-if

            if   (ws-nome-03 = ws-nome-01-t)
            or   (ws-nome-03 = ws-nome-02-t)
            or   (ws-nome-03 = ws-nome-03-t)
                  move "s"                         to ws-valida-nome
                  exit section
            end-if

            .
       2050-exit.
            exit.
      *>===================================================================================
       2051-validar-vendedor section.
       2051.
            move "n"                               to ws-valida-vendedor

            if f-cdVendedor equal zeros
               move "s"                            to ws-valida-vendedor
               exit section
            end-if

            if f-cdVendedor = FK-cod-vendedor
               move "s"                            to ws-valida-vendedor
               exit section
            end-if

            .
       2051-exit.
            exit.
      *>===================================================================================
       3000-finalizacao section.
       3000.

            perform 3010-fechamento-arquivos
            perform 3020-controles-de-tela

            .
       3000-exit.
            exit.
      *>===================================================================================
       3010-fechamento-arquivos section.
       3010.

            close arq-clientes

            .
       3010-exit.
            exit.
      *>===================================================================================
       3020-controles-de-tela section.
       3020.

       exec html
       <script>

           if( ':f-opcao-rel-cliente' == '00' ) {
               document.all.opOrdenacao.focus();
           }

       </script>
       end-exec

            .
       3020-exit.
            exit.
      *>===================================================================================

       copy .\copys\ROTINAS-PADRAO.cpy.
