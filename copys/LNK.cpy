       01 cgi-input is external-form.
           05 f-campos-formulario.
               07 f-opcao                          pic 9(02) identified by "opcao".
               07 f-opcao-menus                    pic 9(02) identified by "opcaoMenus".
               07 f-opcao-cad-cli                  pic 9(02) identified by "opcaoCadCli".
               07 f-opcao-cad-vend                 pic 9(02) identified by "opcaoCadVend".
               07 f-opcao-rel-cliente              pic 9(02) identified by "opcaoRelCliente".
               07 f-login                          pic x(50) identified by "login".
               07 f-senha                          pic x(50) identified by "senha".
               07 f-status-login                   pic x(01) identified by "userLogado".
                   88 f-user-logado                value "S" "s".
           05 f-cad-cliente.
               07 f-id-cliente                     pic 9(07) identified by "txtID".
               07 f-cnpj                           pic 9(14) identified by "txtCNPJ".
               07 f-razao-social                   pic x(40) identified by "txtRazaoSocial".
               07 f-latitude                       pic x(12) identified by "txtLatitude".
               07 f-longitude                      pic x(12) identified by "txtLongitude".
               07 f-arquivo                        pic x(200) identified by "txtArquivo".
               07 f-fk-cod-vendedor                pic 9(07) identified by "txtVendedor".
           05 f-cad-vendedor.
               07 f-id-vendedor                    pic 9(07) identified by "txtIDVendedor".
               07 f-cpf                            pic 9(11) identified by "txtCPF".
               07 f-nome                           pic x(40) identified by "txtNome".
           05 f-rel-cliente.
               07 f-opOrdenacao                    pic 9(02) identified by "opOrdenacao".
               07 f-opTipoClassificacao            pic 9(02) identified by "opTipoClassificacao".
               07 f-cdInicial                      pic 9(07) identified by "cdInicial".
               07 f-cdFinal                        pic 9(07) identified by "cdFinal".
               07 f-txtRelNome                     pic x(40) identified by "txtRelNome".
               07 f-cdVendedor                     pic 9(07) identified by "cdVendedor".

       01 lnk-rotinas.
           02 lnk-path.
               03 lnk-path-copys                   pic x(200).
               03 lnk-path-log                     pic x(200).
               03 lnk-path-dat                     pic x(200).
               03 lnk-path-import                  pic x(200).
               03 lnk-path-tmp                     pic x(200).
           02 lnk-data-hora-maquina.
               03 lnk-data-maquina.
                   04 lnk-maq-ano                  pic 9(04).
                   04 lnk-maq-mes                  pic 9(02).
                   04 lnk-maq-dia                  pic 9(02).
               03 lnk-hora-maquina.
                   04 lnk-maq-hora                 pic 9(02).
                   04 lnk-maq-minu                 pic 9(02).
                   04 lnk-maq-segu                 pic 9(02).
                   04 lnk-maq-mili                 pic 9(02).

