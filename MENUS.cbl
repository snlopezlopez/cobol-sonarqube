      $set preprocess (htmlpp) endp
      $set sourceformat"free"

      *>===================================================================================
       identification division.
       program-id.    Menus.
      *>===================================================================================

      *>   -----------------------------------------------------
      *>               Menus do sistema
      *>   -----------------------------------------------------

      *>===================================================================================
       environment division.
       special-names. decimal-point is comma.
      *>===================================================================================
       input-output section.
       file-control.

      *>===================================================================================
       data division.

      *>===================================================================================
       working-storage section.
       78 versao                       value "1.8".

       copy .\copys\WS-PADRAO.cpy.
       copy .\copys\LNK.cpy.

      *>===================================================================================
       local-storage section.

      *>===================================================================================
       procedure division using cgi-input lnk-rotinas.

      *>===================================================================================
       0000-controle section.
       0000.
            perform 1000-inicializacao.
            perform 2000-processamento.
            perform 3000-finalizacao.
       0000-saida.
            exit program
            stop run.

      *>===================================================================================
       1000-inicializacao section.
       1000.

            move spaces                             to cgi-input
            initialize                              cgi-input
            accept                                  cgi-input
            .
       1000-exit.
            exit.
      *>===================================================================================
       2000-processamento section.
       2000.

       evaluate f-opcao-menus
           when 0
               perform 2010-carrega-menus
           when 1
               call "CAD-CLIENTE" using cgi-input lnk-rotinas
               cancel "CAD-CLIENTE"
           when 2
               call "CAD-VENDEDOR" using cgi-input lnk-rotinas
               cancel "CAD-VENDEDOR"
           when 3
               call "REL-CLIENTES" using cgi-input lnk-rotinas
               cancel "REL-CLIENTES"
           when 4
               call "REL-VENDEDORES" using cgi-input lnk-rotinas
               cancel "REL-VENDEDORES"
           when other
               move "op��o inv�lida"               to ws-mensagem
               perform 8000-mensagem
       end-evaluate

            .
       2000-exit.
            exit.
      *>===================================================================================
       2010-carrega-menus section.
       2010.

       exec html
           <html><title>Menus</title>
       end-exec

            perform 9000-css-pagina
            perform 2020-menus-da-tela
            perform 2030-scripts-da-tela

       exec html
           </html>
       end-exec
            .
       2010-exit.
            exit.
      *>===================================================================================
       2020-menus-da-tela section.
       2020.
       exec html

           <table :html-table>
               <tr>
                   <td class="txtSuperior">Cadastros</td>
                   <td class="txtSuperior">Relat�rios</td>
                   <td class="txtSuperior">Executar</td>
               </tr>

               <tr>
                   <td><input type=button name=botCadClientes value="Cadastro de clientes" onclick="CadCliente()"></td>
                   <td><input type=button name=botRelClientes value="Rel. de clientes" onclick="RelCliente()"></td>
                   <td><input type=button name=botDistClientes value="Executar Distribui��o de Clientes" onclick=""></td>

               </tr>

               <tr>
                   <td><input type=button name=botCadVendedor value="Cadastro de vendedores" onclick="CadVendedor()"></td>
                   <td><input type=button name=botRelVendedores value="Rel. de vendedores" onclick="RelVendedor()"></td>

               </tr>
           </table>


       end-exec
            .
       2020-exit.
            exit.
      *>===================================================================================
       2030-scripts-da-tela section.
       2030.
       exec html
       <script>

           function CadCliente(){
               document.formMenus.opcaoMenus.value = 1;
               document.formMenus.submit();
           }

           function CadVendedor(){
               document.formMenus.opcaoMenus.value = 2;
               document.formMenus.submit();
           }

           function RelCliente(){
               document.formMenus.opcaoMenus.value = 3;
               document.formMenus.submit();
           }

           function RelVendedor() {
               document.formMenus.opcaoMenus.value = 4;
               document.formMenus.submit();
           }

       </script>
       end-exec
            .
       2030-exit.
            exit.
      *>===================================================================================
       3000-finalizacao section.
       3000.

       3000-exit.
            exit.

       copy .\copys\ROTINAS-PADRAO.cpy.

