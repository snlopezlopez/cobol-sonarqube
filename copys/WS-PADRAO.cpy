       78 html-table                               value "border=2 cellspacing=1 cellpadding=5 bordercolor=Gray".

       01 ws-status-arquivos.
            03 ws-resultado-acesso                 pic x(2).
               88 ws-acesso-invalido               value "47", "48", "49".
               88 ws-operacao-ok                   value "00" "02".
               88 ws-eof-arquivo                   value "10".
               88 ws-registro-inexistente          value "23".
               88 ws-registro-existente            value "22".
               88 ws-arquivo-inexistente           value "35".
               88 ws-arquivo-locado                value "9A".
               88 ws-registro-locado               value "9D".

       01 ws-path-arquivos.
           03 ws-status-code                       pic x(02) comp-5.

       01 ws-verifica-arquivos.
           05 ws-file-details.
               07 ws-file-size                     pic x(8) comp-x.
               07 ws-file-date.
                   10 ws-day                       pic x comp-x.
                   10 ws-month                     pic x comp-x.
                   10 ws-year                      pic x(2) comp-x.
               07 ws-file-time.
                   10 ws-hours                     pic x comp-x.
                   10 ws-minutes                   pic x comp-x.
                   10 ws-seconds                   pic x comp-x.
                   10 ws-hundredths                pic x comp-x.

       01 ws-mensagem                              pic x(2000).

