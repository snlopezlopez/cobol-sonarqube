       fd arq-vendedores.
       01 registro-vendedores.
           02 cod-vendedor                         pic 9(07).
           02 cpf                                  pic 9(11).
           02 nome                                 pic x(40).
           02 latitude                             pic x(12).
           02 longitude                            pic x(12).
